import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Route } from '../models/route.model';

@Injectable()
export class RoutesService {
  private routesChangedSource = new Subject <any> ();
  routesChanged$ = this.routesChangedSource.asObservable();

  private routeGraph = {vertex : [], edge : []};

  constructor(private _http : Http) {
    this.loadAllRoutes();
  }

  loadAllRoutes(){
    return this._http.get('assets/testData/response.json')
      .subscribe(res => {
        var result = res.json();

        this.setRoutesToGraph(result.deals);

        this.routesChangedSource.next(result);
      }
    );
  }

  setRoutesToGraph(routes){
    routes.map(route => {
      var index = this.routeGraph.vertex.findIndex(x => x == route.departure);
 
      if(index == -1){
        this.routeGraph.vertex.push(route.departure);
      }

      this.routeGraph.edge.push({
        departure : route.departure, arrival : route.arrival, price : (((100 - route.discount) * route.cost)/100),
          coast : route.cost, discount : route.discount, transport : route.transport, reference : route.reference,
          duration : route.duration, durationSummaryMinutes : this.getSummaryDurationInMinutes(route.duration)
      });
    });
  }

  getShortestPath(fromCity, toCity, weightProperty) {
      var fromProperty = 'departure';
      var toProperty = 'arrival';

      var notVisited = this.routeGraph.vertex.slice(0);
      var track = [];     
			track[fromCity] = {previous : null, price : 0};

			while(true){
				var toOpen = null;
				var bestPrice = Infinity;

				for (var i = 0; i < notVisited.length; ++i) {
					var v = notVisited[i];

					if(track.hasOwnProperty(v) && track[v].price < bestPrice){
						toOpen = v;
						bestPrice = track[v].price;
					}
				}

				if(toOpen == null) return null;
				if(toOpen == toCity) break;

        var minPath = this.getMinPathToSamePoint(
          this.routeGraph.edge, toOpen, fromProperty, toProperty, weightProperty);

				minPath.filter(function(edge) {
					if(toOpen === edge[fromProperty]){
						var currentPrice = track[toOpen].price + edge[weightProperty];
						var nextNode = edge[toProperty];

						if(!track.hasOwnProperty(nextNode) || track[nextNode] > currentPrice){
              track[nextNode] = { previous : toOpen, price : currentPrice, edge : edge}; 
						}
					}
				});

				var index = notVisited.indexOf(toOpen);

				if (index > -1) {
					notVisited.splice(index, 1);
				}
			}

			var result = [];

			while(toCity != null){
        if(track[toCity].edge){
          var route = this.getResultObject(track[toCity].edge)
          result.push(route);
        }

        toCity = track[toCity].previous;
			}

			result.reverse();

			return result;
  }

  getResultObject(route){
    return {
      departure : route['departure'], 
      arrival : route['arrival'],
      price : route['price'],
      cost : route['cost'],
      discount : route['discount'],
      transport : route['transport'],
      reference : route['reference'],
      duration : route['duration']
    }
  }

  getMinPathToSamePoint(edges, toOpen, fromProperty, toProperty, weightProperty){
    var minPath = [];
    //get min path to city
    edges.filter(function(edge) {
      if(toOpen === edge[fromProperty]){
        var index = minPath.findIndex(item => item[fromProperty] == edge[fromProperty] && item[toProperty] == edge[toProperty]);

        if(index === -1){
          minPath.push(edge);
        } else {              
          if(minPath[index][weightProperty] > edge[weightProperty]){
            minPath[index] = edge;
          }
        }
      }
    });

    return minPath;
  }

  getSummaryDurationInMinutes(duration){
    return (Number(duration.h) * 60) + Number(duration.m)
  }
}
