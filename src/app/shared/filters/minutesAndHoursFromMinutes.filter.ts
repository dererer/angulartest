import {Pipe, PipeTransform, Injectable} from '@angular/core';

@Pipe({ name: 'minutesAndHoursFromMinutes' })
export class MinutesAndHoursFromMinutes implements PipeTransform {
  transform(value:any) {
    var result = "";

    var hours = Math.floor(value / 60);          
    if(hours < 10){
      result = "0" + hours + 'h';
    } else {
      result += hours + 'h';
    }

    var minutes = value % 60;
    
    if(minutes ==0){
      result += "0" + minutes;
    }else{
      result += minutes;
    }

    return result;
  }
}