import { Duration } from "./duration.model";

export class Route {
    Arrival : string;
    Departure : string;
    
    Duration : Duration;
    
    Coast : number;
    Discount : number;
    
    Reference : string;
    Transport : string;
}
