import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RoutesService } from './shared/services/routes.service';
import { MinutesAndHoursFromMinutes } from './shared/filters/minutesAndHoursFromMinutes.filter';

@NgModule({
  declarations: [
    AppComponent,
    MinutesAndHoursFromMinutes,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    RoutesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
