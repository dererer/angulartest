import { Component, OnInit } from '@angular/core';
import { RoutesService } from './shared/services/routes.service';
import { Subscription }   from 'rxjs/Subscription';
import { MinutesAndHoursFromMinutes } from './shared/filters/minutesAndHoursFromMinutes.filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  routesSubscription: Subscription;

  from = [];
  to = [];

  toSelectedValue = '';
  fromSelectedValue = '';

  routes = [];
  currancy;

  showLoader = true;

  searchResults = [];

  searchCheapestSelected = true;
  searchFastestSelected = false;

  errorResult;

  tripPrice = 0;
  tripDuration = 0;

  constructor(private _routesService : RoutesService){}

  ngOnInit(){
    this.routesSubscription = this._routesService.routesChanged$
    .subscribe(
      routes => {
        if(routes){
          this.routes = routes.deals;
          this.currancy = routes.currancy;

          this.setCitiesToSelect(this.to, this.routes);
          this.setCitiesFromSelect(this.from, this.routes);

          this.showLoader = false;
        }
    });
  }
  
  searchFastestClick(){
    this.searchCheapestSelected = false;
    this.searchFastestSelected = true;
  }

  searchCheapestClick(){
    this.searchCheapestSelected = true;
    this.searchFastestSelected = false;
  }

  setCitiesFromSelect(field, routes){
    routes.map(route => {
      var fromIndex = field.findIndex(x => x == route.departure);

      if(fromIndex == -1){
        field.push(route.departure);
      }

      field = this.sortCityByName(field);
    });
  }

  setCitiesToSelect(field, routes){
    routes.map(route => {
      var fromIndex = field.findIndex(x => x == route.arrival);

      if(fromIndex == -1){
        field.push(route.arrival);
      }

      field = this.sortCityByName(field);
    });
  }

  search(){
    if(this.toSelectedValue == this.fromSelectedValue && this.toSelectedValue != ''){
      this.errorResult = "Please select another city";
      this.searchResults = [];

      return;
    }
    
    if(this.toSelectedValue && this.fromSelectedValue){
      var weightProperty;

      if(this.searchCheapestSelected){
        weightProperty = 'price';
      } else {
        weightProperty = 'durationSummaryMinutes';
      }

      this.searchResults = this._routesService.getShortestPath(this.fromSelectedValue,  this.toSelectedValue, weightProperty);
      
      if(this.searchResults){
        this.errorResult = '';
        this.setTripSummary(this.searchResults);
      } else {
        this.errorResult = "Sorry path not found";
      }  
    } else {
      this.errorResult = "Please select city";
      this.searchResults = [];
    }
  }

  setTripSummary(trips){
    this.tripPrice = 0;
    this.tripDuration = 0;

    trips.map(trip => {
      this.tripPrice += trip.price;
      this.tripDuration += (Number(trip.duration.h) * 60) + Number(trip.duration.m);
    })
  }

  sortCityByName(cities){
    return cities.sort(function(a, b){

      var nameA = a.toLowerCase();
      var nameB = b.toLowerCase();

      if (nameA < nameB){
        return -1;
      }
       
      if (nameA > nameB){
        return 1;
      }
       
      return 0;
     });
  }

  clearSearchButtonClick(){
    this.fromSelectedValue = '';
    this.toSelectedValue = '';
    this.searchResults = [];
    this.searchCheapestSelected = true;
    this.searchFastestSelected = false;
  }
}